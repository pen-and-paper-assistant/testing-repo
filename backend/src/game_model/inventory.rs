use crate::game_model::item::Item;
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

#[derive(Debug, Default, Deserialize, Serialize, ToSchema)]
pub struct Inventory {
    items: Vec<Item>,
    size: i32,
}

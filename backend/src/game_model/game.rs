use crate::util::Png;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use uuid::Uuid;
use crate::game_model::attributes::Attributes;
use crate::game_model::character::Character;

#[derive(utoipa::ToSchema, Debug, Default, Deserialize, Serialize)]
pub struct Game {
    uid: Uuid,
    name: String,
    characters: Vec<Character>,
    attributes: Attributes,
}

impl Game {
    pub fn new(name: &str, attributes: Vec<&str>) -> Game {
        Game{
            uid: Uuid::new_v4(),
            name: name.to_string(),
            characters: Default::default(),
            attributes: Attributes::from_vec(attributes),
        }
    }
}

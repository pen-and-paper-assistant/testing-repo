use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use utoipa::ToSchema;

#[derive(Debug, Default, Clone, Deserialize, Serialize, PartialEq, ToSchema)]
pub struct Attributes {
    available_points: i32,
    attributes: HashMap<String, i32>,
}

impl Attributes {
    pub fn from_vec(attribute_vector: Vec<&str>) -> Attributes {
        let mut attributes = HashMap::new();
        attribute_vector.into_iter().for_each(|value| {
            attributes.insert(value.to_owned(), 0);
        });

        Attributes {
            available_points: 20,
            attributes,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::game_model::attributes::Attributes;
    use std::collections::HashMap;

    #[test]
    fn attributes_from_vec_success() {
        let attribute_vector = vec!["strength", "intelligence"];
        let attributes = Attributes::from_vec(attribute_vector);
        let map = HashMap::from([("strength".to_string(), 0), ("intelligence".to_string(), 0)]);
        let example = Attributes {
            available_points: 20,
            attributes: map,
        };
        assert_eq!(example, attributes);
    }
}

// #[derive(Debug, Deserialize, Serialize)]
// pub struct Attribute {
//     name: String,
//     points: i32,
// }

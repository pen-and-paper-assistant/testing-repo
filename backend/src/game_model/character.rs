use crate::game_model::attributes::Attributes;
use crate::game_model::inventory::Inventory;
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

#[derive(Debug, Default, Deserialize, Serialize, ToSchema)]
pub struct Character {
    name: String,
    attributes: Attributes,
    player: bool,
    inventory: Inventory,
    health: i32,
    gold: i32,
}
impl Character {
    pub fn get_health(&self) -> i32 {
        self.health
    }

    pub fn set_health(&mut self, new_health: i32) {
        self.health = new_health;
    }

    pub fn get_gold(&self) -> i32 {
        self.gold
    }

    pub fn set_gold(&mut self, new_gold: i32) {
        self.gold = new_gold;
    }
}
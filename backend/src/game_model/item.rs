use crate::util::Png;
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;
use uuid::Uuid;

#[derive(Debug, Deserialize, Serialize, ToSchema)]
pub struct Item {
    id: Uuid,
    name: String,
    description: String,
}

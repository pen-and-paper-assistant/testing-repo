#![allow(unused)] // TODO: remove later
use serde::{Deserialize, Serialize};

use crate::util::Png;
// use crate::util::Uid;
use uuid::Uuid;

pub mod game;
pub mod character;
pub mod attributes;
pub mod inventory;
pub mod item;

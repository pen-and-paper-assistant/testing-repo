#![allow(unused)]

use std::collections::HashMap;
// TODO: remove later
use crate::error::GeneralError;
use crate::game_model::attributes::Attributes;
use crate::game_model::character::Character;
use crate::game_model::game::Game;
use axum::{http::Response, Json};
use serde_json::Value;
use uuid::Uuid;

mod repository {
    use crate::error::GeneralError;
    use crate::game_model::game::Game;

    pub fn save(game: Game) -> Result<Game, GeneralError> {
        // TODO
        Ok(game)
    }
}

pub fn get_games() -> Vec<(String, Uuid)> {
    // TODO
    vec![
        ("greed island".to_string(), Uuid::new_v4()),
        ("castlevania".to_string(), Uuid::new_v4()),
        ("kaesekuchen".to_string(), Uuid::new_v4()),
        ("musterspiel".to_string(), Uuid::new_v4()),
    ]
}

pub fn get_game_by_id(id: Uuid) -> Game {
    // TODO
    Game::default()
}

pub fn get_attributes(id: Uuid) -> Attributes {
    //TODO
    Attributes::default()
}

pub fn create_game(name: &str, attributes: Vec<&str>) -> Result<Game, GeneralError> {
    let game = Game::new(name, attributes);
    repository::save(game)
}

pub fn update_character_health(game_id: Uuid, character_id: Uuid, health: i32) -> Character {
    // TODO
    let mut character = Character::default();
    character.set_health(health);
    character
}

pub fn update_character_gold(game_id: Uuid, character_id: Uuid, gold: i32) -> Character {
    // TODO
    let mut character = Character::default();
    character.set_gold(gold);
    character
}
pub fn update_character_attributes(game_id: Uuid, character_id: Uuid, attributes: HashMap<String, i32>) -> Character{
    Character::default()
}
pub(crate) fn get_characters(game_id: Uuid) -> Vec<Character> {
    todo!()
}

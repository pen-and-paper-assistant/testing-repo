#![allow(unused)]

use std::collections::HashMap;
use crate::game_manager;
use crate::game_model::character::Character;
use crate::game_model::item::Item;
use axum::extract::{Path, Query};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Json, Response};
use axum::routing::{get, post, put, MethodRouter};
use axum::Router;
use axum_macros::debug_handler;
use serde::Deserialize;
use serde::Serialize;
use serde_json::{json, Value};
use utoipa::ToSchema;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, utoipa::ToSchema)]
pub struct NewGame {
    name: String,
    /// a list of attribute names
    attributes: Vec<String>,
}

/// Create a new game
#[utoipa::path(
    post,
    path = "/api/games",
    request_body = NewGame,
    responses(
    (status = 200, body = [Game]),
    ),
)]
async fn create_game(Json(game): Json<NewGame>) -> Result<Response, Response> {
    let attributes = game.attributes.iter().map(|s| s.as_str()).collect();
    match game_manager::create_game(game.name.as_str(), attributes) {
        Ok(saved_game) => Ok((StatusCode::CREATED, Json(saved_game)).into_response()),
        Err(general_error) => Err((StatusCode::BAD_REQUEST, Json(general_error)).into_response()),
    }
}

/// List all saved games
#[utoipa::path(get, path = "/api/games")]
async fn get_games() -> Response {
    Json(game_manager::get_games()).into_response()
}

/// Search a game by id
#[utoipa::path(get, path = "/api/games/{game_id}", params(("game_id" = Uuid, Path, description = "uuid of a game")))]
async fn get_game_by_id(Path(game_id): Path<Uuid>) -> Response {
    Json(game_manager::get_game_by_id(game_id)).into_response()
}

/// Get all players of a game
#[utoipa::path(get, path = "/api/games/{game_id}/players")]
async fn get_players(Path(game_id): Path<Uuid>) -> Response {
    Json(game_manager::get_game_by_id(game_id)).into_response()
}

/// Get all characters
#[utoipa::path(get, path = "/api/games/{game_id}/characters")]
async fn get_characters(Path(game_id): Path<Uuid>) -> Response {
    Json(game_manager::get_characters(game_id)).into_response()
}

/// Add a character
#[utoipa::path(post, path = "/api/games/{game_id}/characters", request_body = Character)]
async fn create_character(
    Path(game_id): Path<Uuid>,
    Json(character): Json<Character>,
) -> Response {
    todo!()
}

/// Get a character
#[utoipa::path(get, path = "/api/games/{game_id}/characters/{character_id}")]
async fn get_character(Path(game_id): Path<Uuid>) -> Response {
    Json(game_manager::get_game_by_id(game_id)).into_response()
}

/// Give a character an item
#[utoipa::path(put, path = "/api/games/{game_id}/characters/{character_id}/items", request_body = Vec<Item>)]
async fn give_item_to_character(
    Path((game_id, character_id)): Path<(Uuid, Uuid)>,
    Json(items): Json<Vec<Item>>,
) -> Response {
    todo!()
}

/// Get a list of all Attributes
#[utoipa::path(get, path = "/api/games/{game_id}/attributes")]
async fn get_attributes(Path(game_id): Path<Uuid>) -> Response {
    Json(game_manager::get_attributes(game_id)).into_response()
}

/// Set a characters attributes
#[utoipa::path(
    put,
    path = "/api/games/{game_id}/characters/{character_id}/attributes",
    request_body = HashMap<String, i32>,
    responses(
    (status = 200, body = [Character]),
    ),
)]
async fn update_character_attributes(
    Path((game_id, character_id)): Path<(Uuid, Uuid)>,
    Json(attributes): Json<HashMap<String, i32>>
) -> Response {
    Json(game_manager::update_character_attributes(
        game_id,
        character_id,
        attributes
    ))
    .into_response()
}

/// Set a characters health
#[utoipa::path(put, path = "/api/games/{game_id}/characters/{character_id}/health", request_body = i32)]
async fn update_character_health(
    Path((game_id, character_id)): Path<(Uuid, Uuid)>,
    Json(health): Json<i32>,
) -> Response {
    Json(game_manager::update_character_health(
        game_id,
        character_id,
        health,
    ))
    .into_response()
}

/// Set a characters gold
#[utoipa::path(put, path = "/api/games/{game_id}/characters/{character_id}/gold", request_body = i32)]
async fn update_character_gold(
    Path((game_id, character_id)): Path<(Uuid, Uuid)>,
    Json(gold): Json<i32>,
) -> Response {
    Json(game_manager::update_character_gold(
        game_id,
        character_id,
        gold,
    ))
    .into_response()
}

#[utoipa::path(get, path = "/api/games/{game_id}/invitelink")]
async fn get_invitelink(Path(game_id): Path<Uuid>) -> Response {
    todo!()
}

pub fn api_router() -> Router {
    Router::new()
        .route("/games", get(get_games).post(create_game))
        .route("/games/:game_id", get(get_game_by_id))
        .route("/games/:game_id/players", get(get_players))
        .route(
            "/games/:game_id/characters",
            get(get_characters).post(create_character),
        )
        .route("/games/:game_id/invitelink", get(get_invitelink))
        .route(
            "/games/:game_id/characters/:character_id",
            get(get_character),
        )
        .route(
            "/games/:game_id/characters/:character_id/items",
            put(give_item_to_character),
        )
        .route("/games/:game_id/attributes", get(get_attributes))
        .route(
            "/games/:game_id/characters/:character_id/attributes",
            put(update_character_attributes),
        )
        .route(
            "/games/:game_id/characters/:character_id/health",
            put(update_character_health),
        )
        .route(
            "/games/:game_id/characters/:character_id/gold",
            put(update_character_gold),
        )
}

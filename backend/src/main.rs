use std::net::SocketAddr;

use axum::Router;
// use tokio::net::unix::SocketAddr;
use tower_http::services::ServeDir;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

mod error;
mod game_manager;
mod game_model;
mod routes;
mod util;

#[tokio::main]
async fn main() {
    #[derive(OpenApi)]
    #[openapi(
    paths(
        routes::create_game,
        routes::get_game_by_id,
        routes::get_players,
        routes::get_characters,
        routes::create_character,
        routes::get_character,
        routes::give_item_to_character,
        routes::get_attributes,
        routes::update_character_attributes,
        routes::update_character_health,
        routes::update_character_gold,
    ),
    components(
        schemas(
            routes::NewGame,
            game_model::game::Game,
            game_model::attributes::Attributes,
            game_model::character::Character,
            game_model::item::Item,
            game_model::inventory::Inventory,
        )
    ),)]
    struct ApiDoc;

    // build address to serve the game master frontend on
    let address: SocketAddr = match cfg!(debug_assertions) {
        true => SocketAddr::from(([127, 0, 0, 1], 8080)),
        false => SocketAddr::from(([127, 0, 0, 1], 0))
    };

    let app = Router::new()
        .merge(SwaggerUi::new("/api/openapi").url("/api-docs/openapi.json", ApiDoc::openapi()))
        .nest_service("/", ServeDir::new("../frontend/build"))
        .nest("/api", routes::api_router());

    let server = axum::Server::bind(&address).serve(app.into_make_service());
    let bound_address = server.local_addr();
    println!("Listening on http://{}", bound_address);

    if !cfg!(debug_assertions) {
        webbrowser::open(format!("http://{bound_address}").as_str()).unwrap();
    }

    server.await.unwrap();
}

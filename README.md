SE2 Sommersemester 2023

# Development

You can build and test the project with `just`.

To get started install `just`
```bash 
cargo install just
```

Afterwards you can use `just --list` to see all available command.

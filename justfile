default:
    just --list

# test the backend and frontend
test:
    just test-backend
    just test-frontend

# test the backend
test-backend:
    cd backend && cargo test

test-frontend:
    cd frontend && echo "frontend tests"

build-backend:
    cd backend && cargo build --release

build-frontend:
    cd frontend && npm i && npm run build

build:
    just build-backend
    just build-frontend

run-frontend:
    cd frontend && npm i && npm run dev -- --open

run-backend:
    cd backend && cargo run

# builds the frontend and starts the backend server
run: build-frontend
    cd backend && cargo run
